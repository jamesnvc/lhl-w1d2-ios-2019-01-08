//
//  Box.m
//  LectureTwoDemo
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Box.h"

@implementation Box

- (instancetype)initWithHeight:(NSInteger)h width:(NSInteger)w depth:(NSInteger)d
{
    self = [super init];
    if (self) {
        // in the initializer, the object isn't finished being set up yet
        // so we don't want to call methods on self quite yet
        // and self.height = ... is actually calling the setHeight: method
        // so we set the instance variables directly
        _height = h;
        _width = w;
        _depth = d;
    }
    return self;
}

- (NSInteger)volume
{
    return self.height * self.width * self.depth;
}

- (NSInteger)cost {
    // $1 per centimeter of the longest dimension
    return 100 * MAX(self.height, MAX(self.width, self.depth));
}

@end
