//
//  Box.h
//  LectureTwoDemo
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Box : NSObject

@property (nonatomic,assign) NSInteger height;
@property (nonatomic,assign) NSInteger width;
@property (nonatomic,assign) NSInteger depth;

- (instancetype)initWithHeight:(NSInteger)h width:(NSInteger)w depth:(NSInteger)d;
- (NSInteger)volume;
- (NSInteger)cost;

@end

NS_ASSUME_NONNULL_END
