//
//  main.m
//  LectureTwoDemo
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "Box.h"

void doThing(int* thing) {
    printf("thing = %p,got %d\n", thing, *thing);
    *thing = 17;
    printf("thing = %d\n", *thing);
    return;
}

int foo(float a, char* b) {
    return 5;
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        [@"Hello world" stringByAppendingString:@"!"];
        char *s = "Hello world"; // c string
        NSLog(@"Hello, World!");
        printf("Hello, world\n");

        NSString *otherString; // declaration of variable
        // make space to store an NSString pointer and let me refer to what's stored at that location by the name "otherString"


        otherString = @"Hello everyone"; //assignment of variable
        // put a pointer to the string "Hello everyone" in the location named "otherString"

        // recommended: combine declaration and assignment
        NSString *myString = @"hello there";

        int x = 0;           NSInteger xx = 0;
        float y = 3.1415;    CGFloat yy = 3.1415;

        
        doThing(&x);
        printf("After doThing, x = %d\n", x);

        if (x != xx) {
            NSLog(@"Values are different");
        }

        if (xx > yy) {
            NSLog(@"X > Y");
        }

        // you basically never should be checking if a float is == to another float
        // this is because of the way floating-point numbers work; they are inherently imprecise

        NSString *foo;


        // all three ways below do the same thing
        NSDate *now = [[NSDate alloc] init];
        NSDate *d = [NSDate alloc];
        d = [d init];
        NSDate *d2 = [NSDate date];
        NSLog(@"Now = %@, d = %@, d2 = %@", now, d, d2);

        int bar = 0, baz = 1;
        if (1 > 0) {
            baz++;
            bar++;
        }

        int aoeu = 10 * 2 / 3 - 14;

        while (baz < 5) {
            baz *= 2;
        }

        do {
            bar += 1;
        } while (bar < 3);

        for (int i = 0; i < 5; i++) {
            printf("i = %d\n", i);
        }

        // Object example stuff

        Car *car1 = [[Car alloc] init];
        car1.model = @"Prius";

        Car *car2 = [[Car alloc] init];
        car2.model = @"Model S";

        Car *car3 = [[Car alloc] init];
        car3.model = @"M3";

        [car1 drive]; // car1.drive
        [car2 drive];
        [car3 drive];

        NSLog(@"Car 1 = %@", car1);

        // Example for why classes are nice

        // ugh
        NSInteger box1Height = 10;
        NSInteger box1Width = 20;
        NSInteger box1Depth = 15;

        NSInteger box1Volume = box1Height * box1Width * box1Depth;
        NSLog(@"Box 1 volume = %ld", box1Volume);

        NSInteger box2Height = 15, box2Width = 15, box2Depth = 15;
//        NSInteger box2Volume = box2Hei

//        UIColor * box1Color = ...;

        Box *box1 = [[Box alloc] initWithHeight:10 width:20 depth:15];
//        box1.height = 10;
//        box1.width = 20;
//        box1.depth = 15; // same as [box1 setDepth:15];

        box1.depth; // same as [box1 depth];
        NSLog(@"Box 1 volume = %ld, cost = %ld", [box1 volume], [box1 cost]);

        Box *box2 = [[Box alloc] init];
        box2.height = box2.width = box2.depth = 15;
        NSLog(@"Box 2 volume = %ld, cost = %ld", box2.volume, [box2 cost]);

        SEL volumeSelector = @selector(volume);
        [box1 performSelector:volumeSelector]; // [box1 volume];
        [box2 performSelector:volumeSelector];
//        [@"" performSelector:volumeSelector];

    }
    return 0;
}
