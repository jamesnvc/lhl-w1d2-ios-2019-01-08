//
//  Car.m
//  LectureTwoDemo
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Car.h"

@implementation Car

- (void)drive {
    NSLog(@"I'm driving my %@", self.model);
}

- (NSString*)description {
    return [NSString stringWithFormat:@"A %@ car", self.model];
}

@end
