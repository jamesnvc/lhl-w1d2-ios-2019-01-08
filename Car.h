//
//  Car.h
//  LectureTwoDemo
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Car : NSObject

@property (nonatomic,strong) NSString *model;

- (void)drive;

@end

NS_ASSUME_NONNULL_END
